<?php
$errors = [];
$trueLines = [
    '1 4',
    '6 45',
    '-74 22',
    '-5 12'
];

if (isset($_POST['go'])) {
    $file = $_FILES['fileInput'];
    //проверка на присутствие файла
    if ($file['name'] == '') {
        $errors['fileRequired'] = 'Вы не выбрали файл';
        return false;
    }
    //проверка на формат файла
    if ($file['type'] !== 'text/plain') {
        $errors['invalidFileType'] = 'Неверный формат файла';
        return false;
    }
    //вытаскиваем строки файла в массив
    $lines = file($file['tmp_name'], FILE_IGNORE_NEW_LINES);
    //фильтруем массив со строками, оставляем только подходящие строки
    $lines = filterLines($lines, $trueLines);
    //проверяем, были ли вообще подходящие строки
    if (!count($lines)) {
        $errors['noSuitableLines'] = 'В файле нет подходящей строки';
        return false;
    }
    $numbersArr = [];
    foreach ($lines as $line) {
        $numbersArr[] = explode(' ', $line);
    }
    //получение массива с результатами
    $calculatedArr = getCalculatedArray($numbersArr, $_POST['typeOfAction']);
    //отправка файла
    saveAndSendFiles($calculatedArr);
}