<?php require('functions.php') ?>
<?php require('script.php')?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        label {
            display: block;
        }
    </style>
</head>
<body>

<?php displayErrors($errors) ?>
<form action="index.php" method="post" enctype="multipart/form-data">
    <input type="file" name="fileInput">
    <?= radio('typeOfAction', 'multiply', 'multiply', true) ?>
    <?= radio('typeOfAction', 'division', 'division') ?>
    <?= radio('typeOfAction', 'plus', 'plus') ?>
    <?= radio('typeOfAction', 'minus', 'minus') ?>
    <button type="submit" name="go" value="1">Go</button>
</form>
</body>
</html>
