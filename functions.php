<?php
//функция дебага
function debug($value)
{
    echo '<pre>';
    print_r($value);
    echo '</pre>';
}
//вспомогательная функция для radio
function checkedOrNot($name, $value, $isDefault = false)
{
    $result = $_POST[$name] == $value ? 'checked' : null;
    if ($isDefault) {
        $result = 'checked';
    }
    return $result;
}
//функция рендеринга radio
function radio($name, $value, $label, $isDefault = false)
{
    $result = '<label>';
    $result .= '<input type="radio" name="' . $name . '" value="' . $value . '" ' . checkedOrNot($name, $value, $isDefault) . '>';
    $result .= $label;
    $result .= '</label>';

    return $result;
}

//функция для вывода ошибок
function displayErrors($errors)
{
    if (!is_array($errors)) {
        return false;
    }
    if (count($errors) == 0) {
        return false;
    }
    foreach ($errors as $error) {
        echo '<div style="color: red;">' . $error . '</div>';
    }
}
//функция для фильтрации подходящих строк
function filterLines($lines, $trueLines)
{
    $result = [];
    foreach ($lines as $line) {
        if (in_array($line, $trueLines)) {
            $result[] = $line;
        }
    }
    return $result;
}
//функция для получения массива с готовыми результатами, в зависимости от типа операции
function getCalculatedArray($numbersArr, $type)
{
    $result = [];
    foreach ($numbersArr as $numbers) {
        switch ($type) {
            case 'plus':
                $result[] = $numbers[0] + $numbers[1];
                break;
            case 'minus':
                $result[] = $numbers[0] - $numbers[1];
                break;
            case 'multiply':
                $result[] = $numbers[0] * $numbers[1];
                break;
            case 'division':
                $result[] = $numbers[0] / $numbers[1];
                break;
        }
    }
    return $result;
}
//сохранение файлов и выдача юзеру
function saveAndSendFiles($array)
{
    $positivePath = 'results/positive.txt';
    $negativePath = 'results/negative.txt';
    $zipPath = 'results/result.zip';
    $positive = fopen($positivePath, 'w+');
    $negative = fopen($negativePath, 'w+');
    foreach ($array as $item) {
        if ($item < 0) {
            fwrite($negative, $item . PHP_EOL);
        } else {
            fwrite($positive, $item . PHP_EOL);
        }
    }
    fclose($positive);
    fclose($negative);
    $zip = new ZipArchive;
    if ($zip->open($zipPath, ZipArchive::CREATE) === TRUE) {
        $zip->addFile($positivePath);
        $zip->addFile($negativePath);
        $zip->close();
    }
    sendFile($zipPath);
    unlink($zipPath);
}
//отправка файла через хэдер, debug не должен использоваться
function sendFile($filePath)
{

    if (file_exists($filePath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));
        readfile($filePath);
    }
}